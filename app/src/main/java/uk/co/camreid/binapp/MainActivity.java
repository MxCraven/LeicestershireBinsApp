package uk.co.camreid.binapp;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.widget.TextView;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView mainText = (TextView) findViewById(R.id.MainText);
        setDay(mainText);
    }

    void setDay(TextView mainText) {

        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        // full name form of the day
        String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
        mainText.setText(dayOfWeek);


        if (dayOfWeek.equals("Monday")) {
            mainText.setText("Monday, not bin day");
        } else if (dayOfWeek.equals("Tuesday")) {
            mainText.setText("Tuesday, not bin day");
        } else if (dayOfWeek.equals("Wednesday")) {
            mainText.setText("Wednesday, not bin day");
        } else if (dayOfWeek.equals("Thursday")) {
            mainText.setText("Thursday, Bins out lads!");
        } else if (dayOfWeek.equals("Friday")) {
            mainText.setText("Friday, Collection, bins in!");
        } else if (dayOfWeek.equals("Saturday")) {
            mainText.setText("Saturday, not bin day");
        } else if (dayOfWeek.equals("Sunday")) {
            mainText.setText("Sunday, not bin day");
        }

        int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

        if (weekOfYear % 2 == 0) {
            //It's green
            mainText.setBackgroundColor(Color.GREEN);
        } else {
            //It's black
            mainText.setBackgroundColor(Color.BLACK);
        }

    }


}
